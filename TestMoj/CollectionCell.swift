import UIKit

class CollectionCell: UICollectionViewCell {

	var layoutWeight: CGFloat = 1.0

	var number: (() -> (Int))?

	@IBOutlet weak var imageView: UIImageView!

	override func prepareForReuse() {
		setAnchorPoint(anchorPoint: CGPoint(x: 0.5, y: 0.5))
	}
}
