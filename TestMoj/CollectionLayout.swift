import UIKit

class CollectionLayout: UICollectionViewLayout {

	var viewModels: [[CellViewModel]]

	init(viewModels: [[CellViewModel]]) {
		self.viewModels = viewModels

		super.init()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private let padding: CGFloat = 8
	private let numberOfRows = 3
	private var lineHeight: CGFloat {
		return collection.frame.height / CGFloat(numberOfRows)
	}

	private var collection: UICollectionView {
		return collectionView ?? UICollectionView()
	}

	private var collectionWidth: CGFloat {
		return collection.frame.width
	}

	private var collectionHeight: CGFloat {
		return lineHeight * CGFloat(collection.numberOfSections)
	}

	override var collectionViewContentSize: CGSize {
		return CGSize(width: collectionWidth, height: collectionHeight)
	}

	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {

		var attributesArray: [UICollectionViewLayoutAttributes] = []

		for section in 0..<collection.numberOfSections {

			let totalSectionWeight: CGFloat = {
				var sum: CGFloat = 0.0
				(0..<collection.numberOfItems(inSection: section)).forEach {
					sum += CGFloat(viewModels[section][$0].layoutWeight)
				}
				return sum
			}()

			var xOffset: CGFloat = 0.0

			for item in 0..<collection.numberOfItems(inSection: section) {

				let currentIndex = IndexPath(row: item, section: section)

				let viewModel = viewModels[section][item]

				let x = xOffset + padding
				let y = (CGFloat(section) * lineHeight) + padding

				let partOfWeight = viewModel.layoutWeight / totalSectionWeight
				let width = partOfWeight * (collectionWidth - padding) - padding

				let attributes = UICollectionViewLayoutAttributes(forCellWith: currentIndex)
				attributes.frame = CGRect(x: x, y: y, width: width, height: lineHeight - padding)
				attributesArray.append(attributes)

				xOffset += x + width
			}
		}

		return attributesArray
	}
}
