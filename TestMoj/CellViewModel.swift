import UIKit

class CellViewModel {

	let image: UIImage
	let layoutWeight: CGFloat
	var number = 0

	init(image: UIImage, layoutWeight: CGFloat? = 1.0) {
		self.image = image
		self.layoutWeight = layoutWeight ?? 1.0
	}
}
