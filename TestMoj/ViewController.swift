import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var collectionView: UICollectionView!

	let data = [
		[CellViewModel(image: #imageLiteral(resourceName: "image_1")), 						CellViewModel(image: #imageLiteral(resourceName: "image_2"))],
		[CellViewModel(image: #imageLiteral(resourceName: "image_5"))],
		[CellViewModel(image: #imageLiteral(resourceName: "image_6")), 						CellViewModel(image: #imageLiteral(resourceName: "image_3"))],
		[CellViewModel(image: #imageLiteral(resourceName: "image_3")), 						CellViewModel(image: #imageLiteral(resourceName: "image_6"))],
		[CellViewModel(image: #imageLiteral(resourceName: "image_4"), layoutWeight: 0.667), CellViewModel(image: #imageLiteral(resourceName: "image_1"), layoutWeight: 1.0)],
		[CellViewModel(image: #imageLiteral(resourceName: "image_2"))],
		[CellViewModel(image: #imageLiteral(resourceName: "image_5"), layoutWeight: 0.8), 	CellViewModel(image: #imageLiteral(resourceName: "image_6"), layoutWeight: 1.0)],
		[CellViewModel(image: #imageLiteral(resourceName: "image_1"), layoutWeight: 1.0), 	CellViewModel(image: #imageLiteral(resourceName: "image_4"), layoutWeight: 0.8)],
		[CellViewModel(image: #imageLiteral(resourceName: "image_6"))]
	]

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)

		var number = 0
		data.forEach {
			$0.forEach {
				$0.number = number
				number += 1
			}
		}
	}

	override func viewDidLoad() {
		collectionView.collectionViewLayout = CollectionLayout(viewModels: data)
		collectionView.register(UINib(nibName: "CollectionCell", bundle: nil), forCellWithReuseIdentifier: "id")
		collectionView.delegate = self
		collectionView.dataSource = self
	}
}

extension ViewController: UICollectionViewDelegate {

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let pageHeight = scrollView.frame.height
		let offset = scrollView.contentOffset.y
		let shift = offset.truncatingRemainder(dividingBy: pageHeight)
		let shiftValue = shift > pageHeight / 2.0 ? pageHeight - shift : shift
		let thresholdAngle: Double = 50
		let resultValue = min(fabs(Double(shiftValue / 2.0)), thresholdAngle)


		collectionView.visibleCells.forEach {

			$0.layer.transform = CATransform3DRotate(CATransform3DIdentity, 0, 0, 0, 0);
			let collectionCenterX = collectionView.center.x
			let cellX = $0.convert($0.frame.origin, to: collectionView).x / 2.0
			let cellWidth = $0.bounds.width
			let anchorPointX = (collectionCenterX - cellX) / cellWidth
			$0.setAnchorPoint(anchorPoint: CGPoint(x: anchorPointX, y: 1))

			var t = CATransform3DIdentity
			t.m34 = 1.0 / -500.0
			t = CATransform3DRotate(t, CGFloat(resultValue * Double.pi / 180.0), 1, 0, 0);
			$0.layer.transform = t
		}
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard 	let cell = collectionView.cellForItem(at: indexPath) as? CollectionCell,
				let number = cell.number?() else {
			return
		}

		let alert = UIAlertController.init(title: "Номер элемента:", message: String(number), preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .cancel))

		present(alert, animated: true)
	}
}

extension ViewController: UICollectionViewDataSource {

	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return data.count
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return data[section].count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "id", for: indexPath) as! CollectionCell

		let model = data[indexPath.section][indexPath.row]
		cell.layoutWeight = model.layoutWeight
		cell.imageView.image = model.image
		cell.number = { [weak model] in return model?.number ?? 0 }

		return cell
	}
}

extension ViewController {

	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

		coordinator.animate(alongsideTransition: { context in
			self.collectionView.reloadData()
		})
	}
}

